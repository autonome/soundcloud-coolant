'use strict';

(async () => {

  const observer = new MutationObserver(function(mutations) {

    mutations.forEach(function(mutation) {

      // Detect anytime some change of any class happens
      // to any node that has the waveform class.
      // TODO: probably a more efficient way to do this
      if (mutation.oldValue && mutation.oldValue.indexOf('waveform') != -1) {

        //console.log('WAVEFORM KILLERRRRR')

        // Remove all waveforms anywhere on the page.
        // TODO: probably a more efficient way to do this.
        // TODO: probably a way to keep a static waveform visible.
        const nodes = document.querySelectorAll('.waveform__layer')
        for (const node of nodes) {
          node.parentNode.removeChild(node);
        }
      }
    });
  });

  // Observe...
  // * changes to attributes
  // * for all descendants of target
  // * for only the `class` attribute
  // * and capture the old value
  const observerConfig = {
    attributes: true,
    subtree: true,
    attributeFilter: [
      'class'
    ],
    attributeOldValue: true,
  };

  // Target the whole doc since we want to kill the waveform
  // wherever on the site it appears. Eg, is totally different
  // DOM structure on list pages vs track pages.
  // TODO: probably a more efficient way to do this.
  const targetNode = document.body;

  observer.observe(targetNode, observerConfig);

})();
